﻿using System.ComponentModel.DataAnnotations;

namespace MVCDemo.Models
{
    public class User
    {
        public int id { get; set; }

        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "Age")]
        public int? age { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-Mail")]
        public string? email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        public string? phoneNumber { get; set; }
        
        [Required(ErrorMessage = "Username is required.")]
        [Display(Name = "Username")]
        public string username  { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        public string password { get; set; }
    }
}
